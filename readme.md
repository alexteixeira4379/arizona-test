# Laravel Dockerized Application

This project is a Laravel application configured to run with Docker, using Nginx, PHP 8.3, and MySQL 8. The goal is to provide a consistent and easy-to-configure development environment.

## Requirements

- Docker
- Docker Compose

## Notes

- This project did not consider or worry about access levels or roles
- This project did not consider client entities (to connect to the rentals)
- Please import the Postman collection in the root of this repository `Arizona_Test_Sample.postman_collection.json`

## Project Structure

```
project-root/
├── app/                     # Laravel code
│   ├── (Laravel code)
├── docker/
│   ├── nginx/
│   │   ├── default.conf     # Nginx configuration
│   ├── php/
│   │   ├── Dockerfile       # Dockerfile for the PHP service
│   │   ├── local.ini        # Local PHP configurations
│   │   └── xdebug.ini       # Xdebug configurations
└── docker-compose.yml       # Docker Compose configuration
```

## Setup (quickstart)

   ```sh
   git clone https://gitlab.com/public-group-2024/arizona-rent-test.git
   cd arizona-rent-test
   cp -f -R app/.env.example app/.env
   docker-compose up -d --build
   docker-compose exec app composer install
   docker-compose exec app php artisan key:generate
   docker-compose exec app chmod -R 777 storage
   sudo chown -R $(whoami):$(whoami) .
   docker-compose exec app php artisan migrate:fresh --seed
   docker-compose exec app php artisan rabbitmq:consume
   ```
Open your browser and navigate to [http://localhost](http://localhost).

## Postman Collection

You can import the collection `Arizona_Test_Sample.postman_collection.json` to your `Postman` in order to test all requests

## Unit & Feature Tests

   ```sh
   docker-compose exec app php artisan test --filter RentalServiceTest
   docker-compose exec app php artisan test --filter RentalFeatureTest
   ```

## RabbitMQ

You can check: http://localhost:15672/

- Username: `guest`
- Pwd: `guest`

## Test the RabbitMQ publish and Consumer

Send a `POST` request to `/calculate-days-remaining` to calculate the remaining days and publish the messages to RabbitMQ.

Make sure the `docker-compose exec app php artisan rabbitmq:consume` command is running to consume the messages and persist the data in the database.

## Useful Commands

- **Stop the containers:**

  ```sh
  docker-compose down
  ```

- **Rebuild the containers:**

  ```sh
  docker-compose up -d --build
  ```

- **Access the app container:**

  ```sh
  docker-compose exec app bash
  ```

- **Check Nginx logs:**

  ```sh
  docker-compose logs webserver
  ```

## File Structure

- **`docker/php/Dockerfile`**: Defines the PHP environment with Xdebug.
- **`docker/nginx/default.conf`**: Nginx configuration to serve the Laravel application.
- **`docker/php/local.ini`**: Custom PHP configurations.
- **`docker/php/xdebug.ini`**: Xdebug configurations.
