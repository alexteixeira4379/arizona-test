<?php

namespace Tests\Feature;

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Rental;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;

class RentalFeatureTest extends TestCase
{
    use RefreshDatabase;

    public function testCreateRental()
    {
        $data = [
            'client_name' => 'Maria Jose',
            'start_date' => '2023-06-01',
            'end_date' => '2023-06-10',
        ];

        $response = $this->postJson('/api/rentals', $data);

        $response->assertStatus(201)
            ->assertJson([
                'client_name' => 'Maria Jose',
                'start_date' => '2023-06-01',
                'end_date' => '2023-06-10',
                'status' => 'active',
            ]);

        $this->assertDatabaseHas('rentals', $data);
    }

    public function testCreateRentalFailsWhenStartDateIsAfterEndDate()
    {
        $data = [
            'client_name' => 'Maria Jose',
            'start_date' => '2023-06-10',
            'end_date' => '2023-06-01',
        ];

        $response = $this->postJson('/api/rentals', $data);

        $response->assertStatus(422)
            ->assertJson([
                'message' => 'Start date must be before or equal to end date. (and 1 more error)',
            ]);
    }

    public function testCreateRentalFailsWhenClientHasActiveRental()
    {
        Rental::create([
            'client_name' => 'Maria Jose',
            'start_date' => '2023-06-01',
            'end_date' => '2023-06-10',
            'status' => 'active',
        ]);

        $data = [
            'client_name' => 'Maria Jose',
            'start_date' => '2023-06-11',
            'end_date' => '2023-06-20',
        ];

        $response = $this->postJson('/api/rentals', $data);

        $response->assertStatus(400)
            ->assertJson([
                'error' => 'Client already has an active rental.',
            ]);
    }

    public function testGetAllRentals()
    {
        Rental::create([
            'client_name' => 'Maria Jose',
            'start_date' => '2023-06-01',
            'end_date' => '2023-06-10',
            'status' => 'active',
        ]);

        Rental::create([
            'client_name' => 'Jane Doe',
            'start_date' => '2023-06-11',
            'end_date' => '2023-06-20',
            'status' => 'active',
        ]);

        $response = $this->getJson('/api/rentals');

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'client_name',
                        'start_date',
                        'end_date',
                        'status',
                        'created_at',
                        'updated_at',
                    ]
                ]
            ]);
    }

    public function testGetRentalById()
    {
        $rental = Rental::create([
            'client_name' => 'Maria Jose',
            'start_date' => '2023-06-01',
            'end_date' => '2023-06-10',
            'status' => 'active',
        ]);

        $response = $this->getJson('/api/rentals/' . $rental->id);

        $response->assertStatus(200)
            ->assertJson([
                'id' => $rental->id,
                'client_name' => $rental->client_name,
                'start_date' => $rental->start_date,
                'end_date' => $rental->end_date,
                'status' => $rental->status,
            ]);
    }

    public function testGetRentalByIdNotFound()
    {
        $response = $this->getJson('/api/rentals/999');

        $response->assertStatus(404)
            ->assertJson([
                'error' => 'Rental not found',
            ]);
    }

    public function testCancelRental()
    {
        $rental = Rental::create([
            'client_name' => 'Maria Jose',
            'start_date' => '2023-06-01',
            'end_date' => '2023-06-10',
            'status' => 'active',
        ]);

        $response = $this->deleteJson('/api/rentals/' . $rental->id);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Rental cancelled successfully.',
            ]);

        $this->assertDatabaseMissing('rentals', ['id' => $rental->id, 'status' => 'active']);
    }

    public function testCancelRentalNotFound()
    {
        $response = $this->deleteJson('/api/rentals/999');

        $response->assertStatus(404)
            ->assertJson([
                'error' => 'Rental not found',
            ]);
    }

    public function testCheckAvailability()
    {
        Rental::create([
            'client_name' => 'Maria Jose',
            'start_date' => '2023-06-01',
            'end_date' => '2023-06-10',
            'status' => 'active',
        ]);

        $response = $this->postJson('/api/rentals/check-availability', ['client_name' => 'Maria Jose']);

        $response->assertStatus(200)
            ->assertJson([
                'available' => false,
            ]);

        $response = $this->postJson('/api/rentals/check-availability', ['client_name' => 'Jane Doe']);

        $response->assertStatus(200)
            ->assertJson([
                'available' => true,
            ]);
    }
}
