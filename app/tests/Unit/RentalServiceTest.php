<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\DTOs\RentalDTO;
use App\Models\Rental;
use App\Repositories\Interfaces\RentalRepositoryInterface;
use App\Services\RentalService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;

class RentalServiceTest extends TestCase
{
    use RefreshDatabase;

    protected $rentalService;
    protected $rentalRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->rentalRepository = Mockery::mock(RentalRepositoryInterface::class);
        $this->rentalService = new RentalService($this->rentalRepository);
    }

    protected function tearDown(): void
    {
        Mockery::close();
        parent::tearDown();
    }

    public function testCreateRentalSuccess()
    {
        $rentalDTO = new RentalDTO('Maria Jose', '2023-06-01', '2023-06-10');

        $rental = new Rental();
        $rental->client_name = $rentalDTO->client_name;
        $rental->start_date = $rentalDTO->start_date;
        $rental->end_date = $rentalDTO->end_date;
        $rental->status = 'active';

        $this->rentalRepository
            ->shouldReceive('checkAvailability')
            ->once()
            ->with($rentalDTO->client_name)
            ->andReturn(false);

        $this->rentalRepository
            ->shouldReceive('create')
            ->once()
            ->with($rentalDTO)
            ->andReturn($rental);

        $result = $this->rentalService->createRental($rentalDTO);

        $this->assertInstanceOf(Rental::class, $result);
        $this->assertEquals($rentalDTO->client_name, $result->client_name);
        $this->assertEquals($rentalDTO->start_date, $result->start_date);
        $this->assertEquals($rentalDTO->end_date, $result->end_date);
    }

    public function testCreateRentalFailsWhenStartDateIsAfterEndDate()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Start date cannot be after end date.');

        $rentalDTO = new RentalDTO('Maria Jose', '2023-06-10', '2023-06-01');
        $this->rentalService->createRental($rentalDTO);
    }

    public function testCreateRentalFailsWhenClientHasActiveRental()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Client already has an active rental.');

        $rentalDTO = new RentalDTO('Maria Jose', '2023-06-01', '2023-06-10');

        $this->rentalRepository
            ->shouldReceive('checkAvailability')
            ->once()
            ->with($rentalDTO->client_name)
            ->andReturn(true);

        $this->rentalService->createRental($rentalDTO);
    }

    public function testCancelRentalSuccess()
    {
        $rental = new Rental();
        $rental->id = 1;
        $rental->client_name = 'Jose Pereira';
        $rental->start_date = '2023-06-01';
        $rental->end_date = '2023-06-10';
        $rental->status = 'active';

        $this->rentalRepository
            ->shouldReceive('delete')
            ->once()
            ->with($rental->id)
            ->andReturn(true);

        $result = $this->rentalService->cancelRental($rental->id);

        $this->assertTrue($result);
    }

    public function testCancelRentalFailsWhenRentalNotFound()
    {
        $this->rentalRepository
            ->shouldReceive('findById')
            ->with(1)
            ->andReturn(null);

        $this->rentalRepository
            ->shouldReceive('delete')
            ->once()
            ->with(1)
            ->andReturn(false);

        $result = $this->rentalService->cancelRental(1);

        $this->assertFalse($result);
    }
}
