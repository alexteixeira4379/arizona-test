<?php

namespace App\Services;

use App\Repositories\Interfaces\RentalRepositoryInterface;
use Illuminate\Support\Facades\App;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exception\AMQPConnectionClosedException;
use PhpAmqpLib\Exception\AMQPChannelClosedException;

class RabbitMQConsumerService
{
    public function consumeMessages(): void
    {
        $connection = new AMQPStreamConnection(
            env('RABBITMQ_HOST', 'localhost'),
            env('RABBITMQ_PORT', 5672),
            env('RABBITMQ_USER', 'guest'),
            env('RABBITMQ_PASSWORD', 'guest')
        );

        $channel = $connection->channel();

        $channel->queue_declare('days_remaining_queue', false, false, false, false);

        $callback = function (AMQPMessage $msg) {
            $data = json_decode($msg->body, true);
            $this->processMessage($data);
        };

        $channel->basic_consume('days_remaining_queue', '', false, true, false, false, $callback);

        while (true) {
            try {
                $channel->wait();
            } catch (AMQPConnectionClosedException $e) {
                // Reconnect or handle the exception
                break;
            } catch (AMQPChannelClosedException $e) {
                // Handle the channel exception
                break;
            } catch (\Exception $e) {
                // Handle any other exceptions
                break;
            }
        }

        $channel->close();
        $connection->close();
    }

    private function processMessage(array $data): void
    {
        $rental = App::make(RentalRepositoryInterface::class)
            ->findActiveByClientName($data['client_name']);

        if ($rental) {
            $rental->days_remaining = $data['days_remaining'];
            $rental->save();
        }
    }
}
