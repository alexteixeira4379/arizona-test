<?php

namespace App\Services;

use App\DTOs\RentalDTO;
use App\Models\Rental;
use App\Repositories\Interfaces\RentalRepositoryInterface;
use App\Services\Interfaces\RentalServiceInterface;
use Illuminate\Database\Eloquent\Collection;

class RentalService implements RentalServiceInterface
{
    protected $rentalRepository;

    public function __construct(RentalRepositoryInterface $rentalRepository)
    {
        $this->rentalRepository = $rentalRepository;
    }

    public function createRental(RentalDTO $rentalDTO): Rental
    {
        if ($rentalDTO->start_date > $rentalDTO->end_date) {
            throw new \Exception('Start date cannot be after end date.');
        }

        if ($this->rentalRepository->checkAvailability($rentalDTO->client_name)) {
            throw new \Exception('Client already has an active rental.');
        }

        return $this->rentalRepository->create($rentalDTO);
    }

    public function getAllRentals(): Collection
    {
        return $this->rentalRepository->findAll();
    }

    public function getRentalById(int $id): ?Rental
    {
        return $this->rentalRepository->findById($id);
    }

    public function cancelRental(int $id): bool
    {
        return $this->rentalRepository->delete($id);
    }

    public function isClientAvailable(string $client_name): bool
    {
        return !$this->rentalRepository->checkAvailability($client_name);
    }
}
