<?php

namespace App\Services;

use App\Models\Rental;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Carbon\Carbon;

class DaysRemainingService
{
    public function calculateAndPublishRemainingDays(): void
    {
        $rentals = Rental::where('status', 'active')->get();

        foreach ($rentals as $rental) {
            $clientName = $rental->client_name;
            $endDate = Carbon::parse($rental->end_date);
            $daysRemaining = Carbon::now()->diffInDays($endDate, false);

            $messageBody = json_encode([
                'client_name' => $clientName,
                'days_remaining' => $daysRemaining
            ]);

            $this->publishMessage($messageBody);
        }
    }

    private function publishMessage($messageBody): void
    {
        $connection = new AMQPStreamConnection(
            env('RABBITMQ_HOST', 'localhost'),
            env('RABBITMQ_PORT', 5672),
            env('RABBITMQ_USER', 'guest'),
            env('RABBITMQ_PASSWORD', 'guest')
        );

        $channel = $connection->channel();

        $channel->queue_declare('days_remaining_queue', false, false, false, false);

        $msg = new AMQPMessage($messageBody);
        $channel->basic_publish($msg, '', 'days_remaining_queue');

        $channel->close();
        $connection->close();
    }
}
