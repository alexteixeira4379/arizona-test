<?php

namespace App\Services\Interfaces;

use App\DTOs\RentalDTO;
use App\Models\Rental;
use Illuminate\Database\Eloquent\Collection;

interface RentalServiceInterface
{
    public function createRental(RentalDTO $rentalDTO): Rental;
    public function getAllRentals(): Collection;
    public function getRentalById(int $id): ?Rental;
    public function cancelRental(int $id): bool;
    public function isClientAvailable(string $client_name): bool;
}
