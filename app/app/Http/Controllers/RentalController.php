<?php

namespace App\Http\Controllers;

use App\DTOs\RentalDTO;
use App\Http\Requests\RentalRequest;
use App\Http\Resources\RentalResource;
use App\Http\Resources\RentalCollection;
use App\Services\Interfaces\RentalServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class RentalController extends Controller
{
    public function __construct(
        private RentalServiceInterface $rentalService,
    ) {
    }

    public function create(RentalRequest $request): JsonResponse
    {
        $data = $request->only(['client_name', 'start_date', 'end_date']);
        $rentalDTO = new RentalDTO($data['client_name'], $data['start_date'], $data['end_date']);

        try {
            $rental = $this->rentalService->createRental($rentalDTO);

            return response()->json(new RentalResource($rental), 201);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function index(): JsonResponse
    {
        $rentals = $this->rentalService->getAllRentals();
        return response()->json(new RentalCollection($rentals));
    }

    public function show($id): JsonResponse
    {
        $rental = $this->rentalService->getRentalById($id);

        if ($rental) {
            return response()->json(new RentalResource($rental));
        } else {
            return response()->json(['error' => 'Rental not found'], 404);
        }
    }

    public function destroy($id): JsonResponse
    {
        if ($this->rentalService->cancelRental($id)) {
            return response()->json(['message' => 'Rental cancelled successfully.']);
        } else {
            return response()->json(['error' => 'Rental not found'], 404);
        }
    }

    public function checkAvailability(Request $request): JsonResponse
    {
        $client_name = $request->input('client_name');

        return response()->json([
            'available' => $this->rentalService->isClientAvailable($client_name)
        ]);
    }
}
