<?php

namespace App\Http\Controllers;

use App\Services\DaysRemainingService;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class CalculateDaysRemainingController extends BaseController
{
    public function __construct(
        private readonly DaysRemainingService $daysRemainingService,
    ) {
    }

    public function calculateDaysRemaining(): JsonResponse
    {
        $this->daysRemainingService->calculateAndPublishRemainingDays();

        return response()->json(['message' => 'Remaining days calculated and messages published successfully.']);
    }
}
