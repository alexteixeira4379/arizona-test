<?php

namespace App\Repositories;

use App\DTOs\RentalDTO;
use App\Models\Rental;
use App\Repositories\Interfaces\RentalRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class RentalRepository implements RentalRepositoryInterface
{
    public function create(RentalDTO $rentalDTO): Rental
    {
        return Rental::create([
            'client_name' => $rentalDTO->client_name,
            'start_date' => $rentalDTO->start_date,
            'end_date' => $rentalDTO->end_date,
            'status' => 'active'
        ]);
    }

    public function findAll(): Collection
    {
        return Rental::all();
    }

    public function findById(int $id): ?Rental
    {
        return Rental::find($id);
    }

    public function delete(int $id): bool
    {
        $rental = $this->findById($id);
        if ($rental) {
            return $rental->delete();
        }
        return false;
    }

    public function checkAvailability(string $client_name): bool
    {
        return Rental::where('client_name', $client_name)->where('status', 'active')->exists();
    }

    public function findActiveByClientName(string $client_name): ?Rental
    {
        return Rental::where('client_name', $client_name)->where('status', 'active')->first();
    }
}
