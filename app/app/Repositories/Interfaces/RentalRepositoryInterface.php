<?php

namespace App\Repositories\Interfaces;

use App\DTOs\RentalDTO;
use App\Models\Rental;
use Illuminate\Database\Eloquent\Collection;

interface RentalRepositoryInterface
{
    public function create(RentalDTO $rentalDTO): Rental;
    public function findAll(): Collection;
    public function findById(int $id): ?Rental;
    public function delete(int $id): bool;
    public function checkAvailability(string $client_name): bool;
    public function findActiveByClientName(string $client_name): ?Rental;
}
