<?php

namespace App\DTOs;

class RentalDTO
{
    public function __construct(
        public string $client_name,
        public string $start_date,
        public string $end_date
    ) {
    }
}
