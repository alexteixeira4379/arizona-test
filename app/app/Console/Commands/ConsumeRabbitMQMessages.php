<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\RabbitMQConsumerService;

class ConsumeRabbitMQMessages extends Command
{
    protected $signature = 'rabbitmq:consume';
    protected $description = 'Consume messages from RabbitMQ and process them';

    public function __construct(
        private readonly RabbitMQConsumerService $rabbitMQConsumerService
    ) {
        parent::__construct();
    }

    public function handle(): void
    {
        $this->info('Starting RabbitMQ consumer...');
        $this->rabbitMQConsumerService->consumeMessages();
    }
}
