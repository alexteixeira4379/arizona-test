<?php

namespace App\Providers;

use App\Repositories\Interfaces\RentalRepositoryInterface;
use App\Repositories\RentalRepository;
use App\Services\DaysRemainingService;
use App\Services\Interfaces\RentalServiceInterface;
use App\Services\RabbitMQConsumerService;
use App\Services\RentalService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(RentalRepositoryInterface::class, RentalRepository::class);
        $this->app->bind(RentalServiceInterface::class, RentalService::class);

        $this->app->singleton(DaysRemainingService::class, function ($app) {
            return new DaysRemainingService();
        });

        $this->app->singleton(RabbitMQConsumerService::class, function ($app) {
            return new RabbitMQConsumerService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
