<?php

use App\Http\Controllers\CalculateDaysRemainingController;
use App\Http\Controllers\RentalController;
use Illuminate\Support\Facades\Route;

Route::post('/rentals', [RentalController::class, 'create']);
Route::get('/rentals', [RentalController::class, 'index']);
Route::get('/rentals/{id}', [RentalController::class, 'show']);
Route::delete('/rentals/{id}', [RentalController::class, 'destroy']);
Route::post('/rentals/check-availability', [RentalController::class, 'checkAvailability']);
Route::post('/calcular-dias-restantes', [CalculateDaysRemainingController::class, 'calculateDaysRemaining']);
